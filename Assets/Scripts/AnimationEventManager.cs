using UnityEngine;
using System;

public class AnimationEventManager : MonoBehaviour
{
    public Action OnThrow;

    public void Throw()
    {
        OnThrow?.Invoke();
    }
}
