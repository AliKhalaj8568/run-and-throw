using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler<T> : MonoBehaviour where T : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public int size;
        public T prefab;
    }
    public List<Pool> pools;
    public Dictionary<string, Queue<T>> PoolDictionary;
    public static ObjectPooler<T> Instance;

    private void Awake()
    {
        Instance = this;
        PoolDictionary = new Dictionary<string, Queue<T>>();
        
        foreach (Pool pool in pools)
        {
            Queue<T> objPool = new Queue<T>();
            for (int i = 0; i < pool.size; i++)
            {   
                T obj = Instantiate(pool.prefab);
                obj.gameObject.SetActive(false);
                objPool.Enqueue(obj);
            }
            PoolDictionary.Add(pool.tag, objPool);
        }
    }
    public virtual T SpawnFromPool(string tag, Vector3 position, Quaternion rotation, Transform parent)
    {
        if (!PoolDictionary.ContainsKey(tag))
            return null;
        var objectToSpawn = PoolDictionary[tag].Dequeue();

        objectToSpawn.gameObject.SetActive(true);
        var objectTransform = objectToSpawn.transform;
        objectTransform.position = position;
        objectTransform.rotation = rotation;
        objectTransform.parent = parent;

        PoolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }
}
