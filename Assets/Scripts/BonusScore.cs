using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BonusScore : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Score score;

    public IEnumerator MoveToTotalScoreLocation(Vector3 targetPosition)
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();
        var startPos = transform.position;
        var endPos = targetPosition;
        
        float delta = 0;
        while (delta <= 0.3f)
        {
            delta += Time.deltaTime;
            var t = delta * speed;
            var pos = Vector3.Lerp(startPos, targetPosition, t);
            transform.position = pos;

            yield return waitForEndOfFrame;
        }
        gameObject.SetActive(false);
        score.Value += 100;
    }
}
