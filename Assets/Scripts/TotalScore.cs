using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;


public class TotalScore : MonoBehaviour
{
    [SerializeField] private float summingSpeed;
    [SerializeField] private Score score;
    [SerializeField] private Text totalScoreText;
    private void Awake()
    {
        score.Value = 0;
        score.OnValueChange += UpdateUI;
    }
    private void UpdateUI()
    {
        StartCoroutine(AnimateSumming(score.Value));
    }

    private IEnumerator AnimateSumming(float targetNum)
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();
        var currentNum = float.Parse(totalScoreText.text);
        currentNum = (int)currentNum;
        float delta = 0;
        while (true)
        {
            delta += Time.deltaTime;
            currentNum += summingSpeed;
            totalScoreText.text = currentNum.ToString();
            if (currentNum >= targetNum)
            {
                totalScoreText.text = targetNum.ToString();
                break;
            }

            yield return waitForEndOfFrame;
        }
    }
}
