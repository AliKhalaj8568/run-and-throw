using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Platform : MonoBehaviour
{
    public List<Target> platformTargets;
    
    [Header("Components")]
    [SerializeField] private Collider trigger;
    [SerializeField] private Transform nextPlatformLocation;

    private ObjectPooler<Obstacle> _obstaclePooler;
    private ObjectPooler<Platform> _platformPooler;

    private void Awake()
    {
        _obstaclePooler = ObstaclePooler.Instance;
        _platformPooler = PlatformPooler.Instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        foreach (var target in platformTargets)
        {
            PlayerManager.Instance.Targets.Enqueue(target);
        }
        trigger.enabled = false;
        GeneratePlatform();
    }
    private void GeneratePlatform()
    {
        var platform = _platformPooler.SpawnFromPool("Platform", nextPlatformLocation.position, Quaternion.identity, null);
        platform.OnSpawn();
    }
    private Vector3 ObstaclePosition()
    {
        var pos = Vector3.zero;
        var offset = Random.Range(-8, 8);
        pos.z += offset;
        pos.y += 1.14f;
        return pos;
    }
    private void OnSpawn()
    {
        trigger.enabled = true;
        foreach (var target in platformTargets)
        {
            target.renderer.material.color = Color.white;
        }
        var obj = _obstaclePooler.SpawnFromPool("Obstacle", ObstaclePosition(), Quaternion.identity,null);
        obj.transform.position += transform.position;
    }
}
