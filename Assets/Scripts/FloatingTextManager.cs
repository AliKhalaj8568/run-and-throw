using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextManager : MonoBehaviour
{
    [SerializeField] private Transform totalScoreLocation;
    private ObjectPooler<BonusScore> _bonusScorePooler; 

    public static FloatingTextManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    public void ShowText(Vector3 position)
    {
        var score = BonusScorePooler.Instance.SpawnFromPool("BonusScore", Vector3.zero, Quaternion.identity, null);
        var scoreTransform = score.transform;
        scoreTransform.position = Camera.main.WorldToScreenPoint(position);
        scoreTransform.parent = this.transform;
        StartCoroutine(score.MoveToTotalScoreLocation(totalScoreLocation.position));
    }
}
