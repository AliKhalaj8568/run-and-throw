using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Target : MonoBehaviour
{
    [SerializeField] private float offset;
    public Renderer renderer;
    private FloatingTextManager _floatingTextManager;

    private void Start()
    {
        _floatingTextManager = FloatingTextManager.Instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        renderer.material.color = RandomColor();
        var pos = transform.position;
        pos.y += offset;
        _floatingTextManager.ShowText(pos);
    }
    private Color RandomColor()
    {
        var x = Random.value;
        var y = Random.value;
        var z = Random.value;
        var w = 1;
        return new Vector4(x, y, z, w);
    }
}
