using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerState : MonoBehaviour
{
    public abstract PlayerState Tik(PlayerManager playerManager, AnimatorHandler animatorHandler);
    public virtual void EnterState(PlayerManager playerManager, AnimatorHandler animatorHandler) {}
    public virtual void ExitState(PlayerManager playerManager, AnimatorHandler animatorHandler){}
    public virtual void OnInputTriggered(PlayerManager playerManager, AnimatorHandler animatorHandler){}
    public virtual void OnCollisionWithGround(AnimatorHandler animatorHandler) {}
    public virtual void ThrowObject(PlayerManager playerManager) {}
}
