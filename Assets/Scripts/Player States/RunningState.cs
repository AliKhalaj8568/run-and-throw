using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class RunningState : PlayerState
{
    [Header("Stats")] 
    [SerializeField] private float speed;
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float jumpMaxHeight;

    [Header("Components")]
    [SerializeField] private Transform playerRoot;
    
    public override PlayerState Tik(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        playerRoot.position += Vector3.forward * speed * Time.deltaTime; 
        return this;
    }

    public override void EnterState(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        playerManager.PreviousState = this;
        
        StartCoroutine(animatorHandler.SetMoveAmountAnimation(1 , 0.1f));
    }

    public override void ExitState(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        animatorHandler.CurrentMoveAmount = 0;
    }

    public override void OnInputTriggered(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        if (!playerManager.CanJump) return;
        StartCoroutine(AnimateJumping(playerManager));        
        StartCoroutine(animatorHandler.SetMoveAmountAnimation(2 , 0.1f));
    }

    public override void OnCollisionWithGround(AnimatorHandler animatorHandler)
    {
        animatorHandler.CurrentMoveAmount = 1;
    }

    private IEnumerator AnimateJumping(PlayerManager playerManager)
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();
        float delta = 0;
        float interpolation = 0;
        while (interpolation >= 0)
        {
            delta += Time.deltaTime * jumpSpeed;
            interpolation = jumpMaxHeight - MathF.Pow(delta - Mathf.Sqrt(jumpMaxHeight), 2);
            var playerTransform = playerManager.transform;
            var pos = playerTransform.localPosition;
            pos.y = interpolation;
            playerTransform.localPosition = pos;
            
            yield return waitForEndOfFrame;
        }
    }
}
