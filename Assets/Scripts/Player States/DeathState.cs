using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathState : PlayerState
{
    public override PlayerState Tik(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        return this;
    }

    public override void EnterState(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        animatorHandler.PlayTargetAnimation("Die", true);
    }
}
