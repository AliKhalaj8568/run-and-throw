using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingState : PlayerState
{
    [SerializeField] private GameObject idleProjectile;
    [SerializeField] private ThrowObject throwObject;
    [SerializeField] private Transform playerHand;
    [SerializeField] private OnHitTarget hit;
    [SerializeField] private RunningState runningState;

    [Header("Event")]
    [SerializeField] private OnPlayerStateChanges onPlayerStateChanges;
    [SerializeField] private OnThrow onThrow;
    [SerializeField] private OnReadyToThrow onReadyToThrow;
    
    private string Throw = "Throw";
    private PlayerManager _playerManager;
    private ObjectPooler<ThrowObject> _throwObjectPooler;

    private void Start()
    {
        onReadyToThrow.ReadyToThrow += ReadyToThrow;
        _throwObjectPooler = ThrowObjectPooler.Instance;
    }
    public override PlayerState Tik(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        return this;
    }
    public override void EnterState(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        onPlayerStateChanges.OnTransformToThrowState?.Invoke();
        playerManager.PreviousState = this;
        _playerManager = playerManager;
        idleProjectile.SetActive(true);
        playerManager.rigidBody.velocity = Vector3.zero;
        playerManager.CurrentTarget = playerManager.Targets.Dequeue();
        LookAtTarget(playerManager);
    }
    public override void ExitState(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        playerManager.model.forward = Vector3.forward;
        onPlayerStateChanges.OnTransformToRunningState?.Invoke();
    }
    public override void OnInputTriggered(PlayerManager playerManager, AnimatorHandler animatorHandler)
    {
        if (playerManager.IsIneracting) return;
        if (idleProjectile.activeSelf)
        {
            animatorHandler.PlayTargetAnimation(Throw, true);
        }
    }
    public override void ThrowObject(PlayerManager playerManager)
    {
        idleProjectile.SetActive(false);
        var obj = _throwObjectPooler.SpawnFromPool("ThrowObject", playerHand.position, Quaternion.identity, null);
        obj.Target = playerManager.CurrentTarget;
        onThrow.OnThrowObject?.Invoke(obj.transform);
    }

    private void ReadyToThrow()
    {
        if (_playerManager.Targets.Count <= 0)
        {
            _playerManager.CurrentState = runningState;
            return;
        }
        idleProjectile.SetActive(true);
        _playerManager.CurrentTarget = _playerManager.Targets.Dequeue();
        LookAtTarget(_playerManager);
    }

    private void LookAtTarget(PlayerManager playerManager)
    {
        var dir = playerManager.CurrentTarget.transform.position - playerManager.transform.position;
        dir.y = 0;
        playerManager.model.forward = dir;
    }
}
