using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathTextManager : MonoBehaviour
{
    [Header("Components")] 
    [SerializeField] private GameObject youDiedText;
    [SerializeField] private PlayerManager playerManager;

    private void Awake()
    {
        playerManager.OnDeath += ActiveYouDiedText;
    }
    private void Start()
    {
        youDiedText.SetActive(false);
    }
    private void ActiveYouDiedText()
    {
        youDiedText.SetActive(true);        
    }
}
