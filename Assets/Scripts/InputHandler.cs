using System;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public Action OnInputTriggered;
    
    private Vector3 _mousePosOnDown;
    private Vector3 _mousePosOnHold;
    private bool _inputTriggered = false;

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (!_inputTriggered)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _mousePosOnDown = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                _mousePosOnHold = Input.mousePosition;
                if (_mousePosOnHold.y - _mousePosOnDown.y > 50)
                {
                    OnInputTriggered?.Invoke();
                    _inputTriggered = true;
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            _inputTriggered = false;
        }
    }
}
