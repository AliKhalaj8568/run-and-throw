using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    [SerializeField] private OnHitTarget hit;
    public Target Target
    {
        set
        {
            _target = value;
            StartCoroutine(FollowTarget());
        }
        get => _target;
    }
    private Target _target;

    [Header("Stats")]
    [SerializeField] private float maxHeight;
    [SerializeField] private float speed;

    private IEnumerator FollowTarget()
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();
        var startPos = transform.position;
        var endPos = Target.transform.position;
        var midPos = (startPos + endPos) / 2;
        midPos.y += maxHeight;

        float delta = 0;
        while (true)
        {
            delta += Time.deltaTime;
            var t = delta * speed;
            var l1 = Vector3.Lerp(startPos, midPos, t);
            var l2 = Vector3.Lerp(midPos, endPos, t);
            var pos = Vector3.Lerp(l1, l2, t);
            transform.position = pos;
            
            yield return waitForEndOfFrame;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        hit.OnHit?.Invoke(other.transform);
        gameObject.SetActive(false);
    }
}

