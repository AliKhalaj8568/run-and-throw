using System.Collections;
using UnityEngine;

public class AnimatorHandler : MonoBehaviour
{
    public float CurrentMoveAmount
    {
        set => anim.SetFloat(MoveAmount, value);
        get => anim.GetFloat(MoveAmount);
    }

    [Header("Components")]
    public Animator anim;
    [SerializeField] private PlayerManager playerManager;

    private static readonly int MoveAmount = Animator.StringToHash("moveAmount");
    
    public void PlayTargetAnimation(string targetAnimation, bool isInteracting)
    {
        playerManager.IsIneracting = isInteracting;
        anim.CrossFade(targetAnimation , 0.2f);
    }
    private void Awake()
    {
        CurrentMoveAmount = 0;
    }
    public IEnumerator SetMoveAmountAnimation(float targetMoveAmount, float dampTime)
    {
        var waitForEndOfFrame = new WaitForEndOfFrame();
        float delta = 0;
        while (delta <= 0.5f)
        {
            delta += Time.deltaTime;
            anim.SetFloat(MoveAmount, targetMoveAmount, dampTime, Time.deltaTime);
            yield return waitForEndOfFrame;
        }
    }
}
