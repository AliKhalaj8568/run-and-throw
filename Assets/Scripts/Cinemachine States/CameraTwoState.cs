using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTwoState : CameraState
{
    [Header("Events")]
    [SerializeField] private OnThrow onThrow;
    [SerializeField] private OnHitTarget onHit;
    [SerializeField] private OnReadyToThrow readyToThrow;
    
    private void Start()
    {
        onThrow.OnThrowObject += LookAtObject;
        onHit.OnHit += LookAtTargetForTwoSeconds;
    }
    private void LookAtObject(Transform obj)
    {
        virtualCamera.LookAt = obj;
    }
    private void LookAtTargetForTwoSeconds(Transform target)
    {
        virtualCamera.LookAt = target;
        Invoke("LookAtPlayer", 2);
    }
    private void LookAtPlayer()
    {
        virtualCamera.LookAt = PlayerManager.Instance.cameraTwoTargetLook;
        readyToThrow.ReadyToThrow?.Invoke();
    }
}
