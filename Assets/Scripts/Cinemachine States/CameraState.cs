using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public abstract class CameraState : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] protected CinemachineVirtualCamera virtualCamera;
    [SerializeField] protected CinemachineManager cinemachineManager;
    public virtual void Enter(CinemachineManager cinemachineManager)
    {
        cinemachineManager.CameraPreviousState = this;
        virtualCamera.Priority = 1;
    }
    public virtual void Exit(CinemachineManager cinemachineManager)
    {
        virtualCamera.Priority = 0;
    }
}
