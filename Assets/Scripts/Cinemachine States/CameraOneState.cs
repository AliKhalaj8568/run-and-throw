using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOneState : CameraState
{
    [Header("Events")]
    [SerializeField] private OnPlayerStateChanges onPlayerStateChanges;
    
    private void Start()
    {
        onPlayerStateChanges.OnTransformToThrowState += TransformToCamTwo;
    }
    private void TransformToCamTwo()
    {
        cinemachineManager.CameraCurrentState = cinemachineManager.cameraTwoState;
    }
}
