using System.Collections;
using UnityEngine;
using Cinemachine;

public class CinemachineManager : MonoBehaviour
{
    public CameraState CameraCurrentState
    {
        set
        {
            cameraCurrentState = value;
            if (CameraPreviousState != null)
            {
                CameraPreviousState.Exit(this);
            }
            CameraCurrentState.Enter(this);
        }
        get => cameraCurrentState;
    }
    public CameraState CameraPreviousState { set; get; }
    [Header("cameraStates")] 
    [SerializeField] private CameraState cameraCurrentState;
    public CameraOneState cameraOneState;
    public CameraTwoState cameraTwoState;

    [Header("Events")]
    [SerializeField] private OnPlayerStateChanges onPlayerStateChanges;
    [SerializeField] private OnThrow onThrow;
    [SerializeField] private OnHitTarget onHitTarget;
    
    [Header("Components")]
    [SerializeField] private CinemachineVirtualCamera cam1;
    [SerializeField] private CinemachineVirtualCamera cam2;
    [SerializeField] private CinemachineVirtualCamera cam3;
    
    private void Start()
    {
        CameraCurrentState = cameraOneState;
        onPlayerStateChanges.OnTransformToRunningState += TransformToCamOne;
    }

    private void TransformToCamOne()
    {
        CameraCurrentState = cameraOneState;
    }
}
