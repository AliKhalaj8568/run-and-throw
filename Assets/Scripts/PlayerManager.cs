using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public Action OnDeath;
    public Target CurrentTarget { set; get; }
    public Queue<Target> Targets = new Queue<Target>();

    public bool IsIneracting
    {
        set => animatorHandler.anim.SetBool(IsInteracting, value);
        get => animatorHandler.anim.GetBool(IsInteracting);
    }
    public bool CanJump
    {
        get
        {
            var results = Physics.OverlapSphere(groundDetection.position, detectionRadius, groundLayer);
            return results.Length > 0;
        }    
    }
    public PlayerState PreviousState { set; get; }
    public PlayerState CurrentState
    {
        set
        {
            currentState = value;
            if (PreviousState != null)
            {
                PreviousState.ExitState(this, animatorHandler);
            }
            CurrentState.EnterState(this, animatorHandler);
        }
        get => currentState;
    }
    [Header("States")] 
    [SerializeField] private PlayerState currentState;
    [SerializeField] private DeathState deathState;
    [SerializeField] private RunningState runningState;
    [SerializeField] private ThrowingState throwingState;


    [Header("Ground Detection")] 
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Transform groundDetection;
    [SerializeField] private float detectionRadius;
     
    [Header("Components")] 
    public Rigidbody rigidBody;
    public Transform model;
    public Transform cameraTwoTargetLook;
    [SerializeField] private AnimatorHandler animatorHandler;
    [SerializeField] private InputHandler inputHandler;
    [SerializeField] private AnimationEventManager animationEventManager;

    private static readonly int IsInteracting = Animator.StringToHash("isInteracting");
    public static PlayerManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        animationEventManager.OnThrow += OnThrowAnimationEvent;
        inputHandler.OnInputTriggered += TriggerCurrentStateInput;
        CurrentState = runningState;
    }
    private void Update()
    {
        CurrentState.Tik(this, animatorHandler);
    }
    private void TriggerCurrentStateInput()
    {
        CurrentState.OnInputTriggered(this, animatorHandler);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag($"Platform"))
        {
            CurrentState.OnCollisionWithGround(animatorHandler);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))
        {
            CurrentState = deathState;
            OnDeath?.Invoke();
        }
        else
        {
            TransformToThrowingState();
        }
    }
    private void TransformToThrowingState()
    {
        CurrentState = throwingState;
    }
    private void OnThrowAnimationEvent()
    {
        CurrentState.ThrowObject(this);
    }
}
