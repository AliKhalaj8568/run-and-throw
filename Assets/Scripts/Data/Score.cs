using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Score")]
public class Score : ScriptableObject
{
    public Action OnValueChange;

    public float Value
    {
        set
        {
            _value = value;
            OnValueChange?.Invoke();
        }
        get => _value;
    }
    private float _value;
}
