using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Event/OnHitTarget")]
public class OnHitTarget : ScriptableObject
{
    public Action<Transform> OnHit;
}
