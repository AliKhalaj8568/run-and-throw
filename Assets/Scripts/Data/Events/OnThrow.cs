using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Event/OnThrow")]
public class OnThrow : ScriptableObject
{
    public Action<Transform> OnThrowObject;
}
