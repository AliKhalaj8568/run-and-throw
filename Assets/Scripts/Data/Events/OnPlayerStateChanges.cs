using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Event/OnPlayerStateChanges")]
public class OnPlayerStateChanges : ScriptableObject
{
    public Action OnTransformToThrowState;
    public Action OnTransformToRunningState;
}
