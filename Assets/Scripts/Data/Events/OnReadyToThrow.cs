using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Event/OnReadyToThrow")]
public class OnReadyToThrow : ScriptableObject
{
    public Action ReadyToThrow;
}
